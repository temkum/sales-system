<nav>
    <ul class="pagination">
        <li class="page-item">
            <a class="page-link" href="http://127.0.0.1:8000/admin/orders?page=1" rel="prev"
                aria-label="« Previous">‹</a>
        </li>
        <li class="page-item"><a class="page-link" href="http://127.0.0.1:8000/admin/orders?page=1">1</a></li>
        <li class="page-item active" aria-current="page"><span class="page-link">2</span></li>
        <li class="page-item disabled" aria-disabled="true" aria-label="Next »">
            <span class="page-link" aria-hidden="true">›</span>
        </li>
    </ul>
</nav>
